package models;

import java.io.Serializable;

public class PeriodCount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 141309840261424484L;
	
	int years = 0;
	int days = 0;
	int hours = 0;
	int minutes = 0;
	int seconds = 0;
	
	public PeriodCount() {
		
	}

	public PeriodCount(int years, int days, int hours, int minutes, int seconds) {
		this.years = years;
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	
	public PeriodCount(int days, int hours, int minutes, int seconds) {
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	
	public void addYears(int years) {
		this.years+=years;
	}
	
	public void addDays(int days) {
		this.days+=days;
		while (this.days > 365) {
			this.years+=1;
			addDays(-365);
		}
		while (this.days < 0) {
			this.days+=365;
			addYears(-1);
		}
	}
	public void addHours(int hours) {
		this.hours+=hours;
		while (this.hours > 24) {
			this.hours-=24;
			addDays(1);
		}
		while (this.hours < 0) {
			this.hours+=24;
			addDays(-1);
		}
	}
	public void addMinutes(int minutes) {
		this.minutes+=minutes;
		while (this.minutes > 60) {
			this.minutes-=60;
			addHours(1);
		}
		while (this.minutes < 0) {
			this.minutes+=60;
			addHours(-1);
		}
	}
	public void addSeconds(int seconds) {
		this.seconds+=seconds;
		while (this.seconds > 60) {
			this.seconds-=60;
			addMinutes(1);
		}
		while (this.seconds < 0) {
			this.seconds+=60;
			addMinutes(-1);
		}
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
}

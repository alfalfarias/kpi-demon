package models;

import java.io.Serializable;

public class ConnectionRecordModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5595965297875733814L;
	
	public ConnectionRecordModel() {
		 
	}
	public ConnectionRecordModel(String ip_origen, String ip_destino, DateModel executionDateModel, String estado) {
		super();
		this.ip_origen = ip_origen;
		this.ip_destino = ip_destino;
		this.executionDateModel = executionDateModel;
		this.estado = estado;
	}
	
	public ConnectionRecordModel(String ip_origen, String ip_destino, DateModel executionDateModel) {
		super();
		this.ip_origen = ip_origen;
		this.ip_destino = ip_destino;
		this.executionDateModel = executionDateModel;
	}
	String ip_origen;
	String ip_destino;
	DateModel executionDateModel;
	String estado;
	
	public String getIp_origen() {
		return ip_origen;
	}
	public void setIp_origen(String ip_origen) {
		this.ip_origen = ip_origen;
	}
	public String getIp_destino() {
		return ip_destino;
	}
	public void setIp_destino(String ip_destino) {
		this.ip_destino = ip_destino;
	}
	public DateModel getExecutionDateModel() {
		return executionDateModel;
	}
	public void setExecutionDateModel(DateModel executionDateModel) {
		this.executionDateModel = executionDateModel;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}

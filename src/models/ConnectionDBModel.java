package models;

import java.io.Serializable;

public class ConnectionDBModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7952221725447456934L;
	
	public String host=null;
	public String port=null;
	public String database=null;
	public String username=null;
	public String password=null;
	
	public ConnectionDBModel() {
		
	}
	
	public ConnectionDBModel(String host, String port, String database, String username, String password) {
		this.host = host;
		this.port = port;
		this.database = database;
		this.username = username;
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

package models;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.Period;
 
public class MainDataStatic implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1385490229257729867L;

	public static boolean analysis_status = false;
	
	public static PeriodCount periodCount = new PeriodCount(0,13,30,0);

	public static ConnectionDBModel extraccionCDBModel = new ConnectionDBModel("127.0.0.1", "3306", "gramiren", "root", "");
	public static ConnectionDBModel cargaCDBModel = new ConnectionDBModel("127.0.0.1", "3306", "datawherehouse", "root", "");
	
	public static ArrayList<ConnectionRecordModel> connectionRecordsModel = new ArrayList<ConnectionRecordModel>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1008026094004625966L;

		{
			add(new ConnectionRecordModel("64.255.63.35", "14.139.52.17", new DateModel(2018,07,5,1,23,56), "migrado"));
			add(new ConnectionRecordModel("64.255.63.35", "14.139.52.17", new DateModel(2018,07,5,8,55,38), "error de conexion"));
			add(new ConnectionRecordModel("64.255.63.35", "14.139.52.17", new DateModel(2018,07,5,0,59,53), "migrado"));
			add(new ConnectionRecordModel("64.255.63.35", "14.139.52.17", new DateModel(2018,07,5,5,48,2), "migrado"));
			add(new ConnectionRecordModel("64.255.63.35", "14.139.52.17", new DateModel(2018,07,6,12,14,0), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,6,7,57,21), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,6,3,0,0), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,6,0,5,42), "error de conexion"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,6,23,40,12), "error de conexion"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,7,4,31,53), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,9,8,22,15), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2018,07,8,0,9,0), "migrado"));
			add(new ConnectionRecordModel("138.231.223.123", "14.139.52.17", new DateModel(2019,6,21,19,20,30), "migrado"));
		}
	};
	

	public MainDataStatic importData(String path) throws IOException, ClassNotFoundException{
		   FileInputStream fileIn = new FileInputStream(path);
		   ObjectInputStream in = new ObjectInputStream(fileIn);
		   MainDataStatic mainDataStatic = (MainDataStatic) in.readObject();
		   in.close();
		   fileIn.close();
		   return mainDataStatic;
	}
	public static void exportData(String path, MainDataStatic data) throws IOException{
		String format = ".data";
		if (!path.endsWith(".data"))
			path+=format;
		FileOutputStream fileOut = new FileOutputStream(path);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(data);
		out.close();
		fileOut.close();
	}
	
	public static DateModel lastUpdate() {
		DateTime nowDateTime = new DateTime();
		DateModel lastUpdate = new DateModel(nowDateTime.getYear(), nowDateTime.getMonthOfYear(), nowDateTime.getDayOfMonth(), nowDateTime.getHourOfDay(), nowDateTime.getMinuteOfHour(), nowDateTime.getSecondOfMinute());
		if (connectionRecordsModel.size() == 0) {
			return lastUpdate;
		}
		lastUpdate = connectionRecordsModel.get(connectionRecordsModel.size()-1).executionDateModel;
		return lastUpdate;
	}
	public static DateModel nextUpdate() {
		DateTime lastUpdateDateTime = new DateTime(lastUpdate().getYear(), lastUpdate().getMonth(), lastUpdate().getDay(), lastUpdate().getHour(), lastUpdate().getMinute(), lastUpdate().getSecond());
		DateTime nextUpdateDateTime = lastUpdateDateTime.plus(Period.days(periodCount.getDays()))
				.plus(Period.hours(periodCount.getHours()))
				.plus(Period.minutes(periodCount.getMinutes()))
				.plus(Period.seconds(periodCount.getSeconds()));
		DateModel nextUpdateDateModel = new DateModel(nextUpdateDateTime.getYear(), nextUpdateDateTime.getMonthOfYear(), nextUpdateDateTime.getDayOfMonth(), nextUpdateDateTime.getHourOfDay(), nextUpdateDateTime.getMinuteOfHour(), nextUpdateDateTime.getSecondOfMinute());
		return nextUpdateDateModel;
	}
	public static Period nextUpdatePeriod() {
		DateTime nowDateTime = new DateTime();
		DateTime newtUpdateDateTime = new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(), MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute(), MainDataStatic.nextUpdate().getSecond());
						
		Period period = new Period(nowDateTime, newtUpdateDateTime);
		return period;
	}
	
	public static void migrate() {
		System.out.println("Realizando migración");
		DateTime nowDateTime = new DateTime();
		connectionRecordsModel.add(new ConnectionRecordModel("127.0.0.1", "127.0.0.2", new DateModel(nowDateTime.getYear(), nowDateTime.getMonthOfYear(), nowDateTime.getDayOfMonth(), nowDateTime.getHourOfDay(), nowDateTime.getMinuteOfHour(), nowDateTime.getSecondOfMinute())));
	}
	
	public static void main(String[] args) throws IOException, IllegalArgumentException, IllegalAccessException{
//		for (Field field : MainDataStatic.nextUpdate().getClass().getDeclaredFields()) {
//		    field.setAccessible(true);
//		    String name = field.getName();
//		    Object value = field.get(MainDataStatic.nextUpdate());
//		    System.out.printf("%s: %s%n", name, value);
//		}
//		System.out.print("Next: "+MainDataStatic.lastUpdate().days);
		MainDataStatic mainDataStatic = new MainDataStatic();
		String example = "C:\\Users\\Simon Farias\\Desktop\\vendor\\lulu";
		MainDataStatic.exportData(example, mainDataStatic);
	}
	public static boolean isAnalysisStatus() {
		return analysis_status;
	}
	public static void setAnalysisStatus(boolean analysis_status) {
		MainDataStatic.analysis_status = analysis_status;
	}
	public static PeriodCount getPeriodCount() {
		return periodCount;
	}
	public static void setPeriodCount(PeriodCount periodCount) {
		MainDataStatic.periodCount = periodCount;
	}
	public static ConnectionDBModel getExtraccionCDBModel() {
		return extraccionCDBModel;
	}
	public static void setExtraccionCDBModel(ConnectionDBModel extraccionCDBModel) {
		MainDataStatic.extraccionCDBModel = extraccionCDBModel;
	}
	public static ConnectionDBModel getCargaCDBModel() {
		return cargaCDBModel;
	}
	public static void setCargaCDBModel(ConnectionDBModel cargaCDBModel) {
		MainDataStatic.cargaCDBModel = cargaCDBModel;
	}
	public static ArrayList<ConnectionRecordModel> getConnectionRecordsModel() {
		return connectionRecordsModel;
	}
	public static void setConnectionRecordsModel(ArrayList<ConnectionRecordModel> connectionRecordsModel) {
		MainDataStatic.connectionRecordsModel = connectionRecordsModel;
	}
}

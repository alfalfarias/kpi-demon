package controllers;

public class ConexionDB {
	public String host=null;
	public String port=null;
	public String database=null;
	public String username=null;
	public String password=null;
	
	public ConexionDB() {
		
	}
	
	public ConexionDB(String host, String port, String database, String username, String password) {
		this.host = host;
		this.port = port;
		this.database = database;
		this.username = username;
		this.password = password;
	}
}

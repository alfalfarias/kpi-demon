package examples;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
 
/**
 *
 * @author Jonathan
 */
public class Main {
    public static void main(String args[]) throws Exception{
//        TrayIcon icono = new TrayIcon(Toolkit.getDefaultToolkit().getImage(EtlJFrame.class.getResource("/img/data-storage.png")));
        TrayIcon icono = new TrayIcon(Toolkit.getDefaultToolkit().getImage(System.getProperty("user.dir") + "\\src\\img\\settings.png"),"ETL - Extracci�n, transformaci�n y carga",crearMenu());
        SystemTray.getSystemTray().add(icono);
        Thread.sleep(1000);
        icono.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(null, "Visita JonathanMelgoza.com/blog", "Atencion!", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        icono.displayMessage("ETL - En ejecuci�n", "Clickea aqu� para m�s informaci�n", TrayIcon.MessageType.INFO);
    }
    
    public static PopupMenu crearMenu(){
        PopupMenu menu = new PopupMenu();
        MenuItem analisisMenuItem = new MenuItem("Ver estado del an�lisis");
        MenuItem salirMenuItem = new MenuItem("Salir");

        analisisMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
				JOptionPane.showMessageDialog(
						   null,
						   "ventana: ");
            }
        });
        salirMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });

        menu.add(analisisMenuItem);
        menu.add(salirMenuItem);
        return menu;
    }
}
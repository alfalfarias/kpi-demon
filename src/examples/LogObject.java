package examples;

import java.lang.reflect.Field;

import models.MainDataStatic;

public class LogObject {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		for (Field field : MainDataStatic.lastUpdate().getClass().getDeclaredFields()) {
		    field.setAccessible(true);
		    String name = field.getName();
		    Object value = field.get(MainDataStatic.lastUpdate());
		    System.out.printf("%s: %s%n", name, value);
		}
	}
}

package examples;

public class RunnableDemo implements Runnable {
	private Thread t;
	   private String threadName;
	   
	   RunnableDemo( String name) {
	      threadName = name;
	      System.out.println("Creating " +  threadName );
	   }
	   
	   public void run() {
	      System.out.println("Running " +  threadName );
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread: " + threadName + ", " + i);
	            // Let the thread sleep for a while.
	            Thread.sleep(50);
	         }
	      }catch (InterruptedException e) {
	         System.out.println("Thread " +  threadName + " interrupted.");
	      }
	      System.out.println("Thread " +  threadName + " exiting.");
	   }
	   
	   public void start () {
	      System.out.println("Starting " +  threadName );
	      if (t == null) {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	   }

	   public static void main(String args[]) {
	      @SuppressWarnings("unused")
		T1 R1 = new T1();
	      

	      @SuppressWarnings("unused")
		T2 R2 = new T2();
	   }   
	}
class T1 implements Runnable {
	private Thread t;
	public T1() {
        t = new Thread (this);
        t.start ();
	}
	@Override
	public void run() {
	      System.out.println("Running T1"  );
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread: T1." + i);
	            // Let the thread sleep for a while.
	            Thread.sleep(50);
	         }
	      }catch (InterruptedException e) {
	         System.out.println("Thread T1 interrupted.");
	      }
	      System.out.println("Thread T1 exiting.");
	}
	
}
class T2 implements Runnable {
	private Thread t;
	public T2() {
        t = new Thread (this);
        t.start ();
	}
	@Override
	public void run() {
	      System.out.println("Running T2"  );
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread: T2." + i);
	            // Let the thread sleep for a while.
	            Thread.sleep(50);
	         }
	      }catch (InterruptedException e) {
	         System.out.println("Thread T2 interrupted.");
	      }
	      System.out.println("Thread T2 exiting.");
	}
	
}
package examples;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.ConnectionDBModel;
import models.MainDataStatic;


public class MySQL {
	
    protected static Connection connection;
    static ResultSet resultSet = null;
    static Statement statement = null;
    ConnectionDBModel connectionDBModel = new ConnectionDBModel();
	
	public static boolean pingConnect(ConnectionDBModel connectionDBModel) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://"+connectionDBModel.getHost()+":"+connectionDBModel.getPort()+"/" + connectionDBModel.getDatabase(), connectionDBModel.getUsername(), connectionDBModel.getPassword());
		
		statement = connection.createStatement();
		resultSet=statement.executeQuery("SELECT 1;");  
		while(resultSet.next())  
		System.out.println("Hola: "+resultSet.getInt(1)); 
		
		statement.close();
        connection.close();
		return true;
	}
	public static void main(String args[]) {
		try {
			MySQL.pingConnect(MainDataStatic.getExtraccionCDBModel());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


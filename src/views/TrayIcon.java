package views;

import java.awt.EventQueue;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class TrayIcon {
	public TrayIcon() {
		PopupMenu menu = new PopupMenu();
        MenuItem analisisMenuItem = new MenuItem("Ver estado del an�lisis");
        MenuItem salirMenuItem = new MenuItem("Salir");

        analisisMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
            	EventQueue.invokeLater(new Runnable() {
        			public void run() {
        				LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
        				try {
        					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        				} catch (Exception e) {
        					try {
        						UIManager.setLookAndFeel(lookAndFeel);
        					} catch (UnsupportedLookAndFeelException e1) {
        						JOptionPane.showMessageDialog(
        								   null,
        								   "Error: "+e1.getMessage().toString());
        					}
        				} 
        				
        				try {
        					EtlJFrame frame = new EtlJFrame();
        					frame.setVisible(true);
        				} catch (Exception e) {
        					JOptionPane.showMessageDialog(
        							   null,
        							   "Error: "+e.getMessage().toString());
        				}
        			}
        		});

            }
        });
        salirMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });

        menu.add(analisisMenuItem);
        menu.add(salirMenuItem);
	}
}

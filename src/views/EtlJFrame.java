package views;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.LookAndFeel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.joda.time.DateTime;
import org.joda.time.Period;

import models.ConnectionRecordModel;
import models.MainDataStatic;

import javax.swing.border.TitledBorder;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JButton;

public class EtlJFrame extends JFrame  implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 286000821846657631L;
	private JPanel contentPane;
	private JTable connectionRecordsTable;
	public JToggleButton btnAnalysisStatus;

    Thread hilo = new Thread( this );
    public JLabel lblTiempoRestante;
    public JPanel panel;
    private JLabel lblLastUpdate;
    private JLabel lblNextUpdate;
    private JScrollPane scrollPane;
    public JButton btnModificarPeriodicidad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				} catch (Exception e) {
					try {
						UIManager.setLookAndFeel(lookAndFeel);
					} catch (UnsupportedLookAndFeelException e1) {
						JOptionPane.showMessageDialog(
								   null,
								   "Error: "+e1.getMessage().toString());
					}
				} 
				
				try {
					EtlJFrame frame = new EtlJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(
							   null,
							   "Error: "+e.getMessage().toString());
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EtlJFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(EtlJFrame.class.getResource("/img/settings.png")));
		setTitle("ETL - Extracci\u00F3n, transformaci\u00F3n y carga");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 703, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
		);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Registro hist�rico de operaciones de extracci�n, transformaci�n y carga", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel panel_2 = new JPanel();
		
		JPanel panel_3 = new JPanel();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
							.addGap(2)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(16)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		lblLastUpdate = new JLabel("\u00DAltima actualizaci\u00F3n: 0000-00-00, Hora: 00:00.");
		lblLastUpdate.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblNextUpdate = new JLabel("Pr\u00F3xima actualizaci\u00F3n: 0000-00-00, Hora: 00:00.");
		lblNextUpdate.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblTiempoRestante = new JLabel("Tiempo restante: 0 D\u00EDas,  0 horas y  0 minutos con 0 segundos.");
		lblTiempoRestante.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_3.createSequentialGroup()
					.addGap(1)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addComponent(lblTiempoRestante, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
						.addComponent(lblNextUpdate, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
						.addComponent(lblLastUpdate, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panel_3.createSequentialGroup()
					.addComponent(lblLastUpdate)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNextUpdate)
					.addGap(10)
					.addComponent(lblTiempoRestante)
					.addContainerGap(1, Short.MAX_VALUE))
		);
		panel_3.setLayout(gl_panel_3);
		
		JLabel lblNewLabel = new JLabel("Migraciones - ETL");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		
		JLabel lblEstadoDelAnlisis = new JLabel("Estado del an\u00E1lisis:");
		lblEstadoDelAnlisis.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		btnAnalysisStatus = new JToggleButton("Estado de an�lisis");
		btnAnalysisStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainDataStatic.setAnalysisStatus(!MainDataStatic.isAnalysisStatus());
				if (MainDataStatic.isAnalysisStatus() == true) {
					btnAnalysisStatus.setText("En ejecuci�n");
					JOptionPane.showMessageDialog(
								panel,
							   "Se ha reanudado la ejecuci�n del ETL");
				}
				else {
					btnAnalysisStatus.setText("ETL en pausa");
					JOptionPane.showMessageDialog(
								panel,
							   "Se ha detenido la ejecuci�n del ETL");
				}
			}
		});
		

		btnAnalysisStatus.setSelected(MainDataStatic.isAnalysisStatus());
		if (btnAnalysisStatus.isSelected()) {
			btnAnalysisStatus.setText("En ejecuci�n");
//			JOptionPane.showMessageDialog(
//					   panel,
//					   "Se ha reanudado la ejecuci�n del ETL");
		}
		else {
			btnAnalysisStatus.setText("ETL en pausa");
//			JOptionPane.showMessageDialog(
//					   panel,
//					   "Se ha detenido la ejecuci�n del ETL");
		}
		
		btnModificarPeriodicidad = new JButton("Modificar periodicidad del ETL");
		btnModificarPeriodicidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EtlPeriodicityJDialog etlPeriodicityJDialog=new EtlPeriodicityJDialog();
				etlPeriodicityJDialog.setLocationRelativeTo(panel);
				etlPeriodicityJDialog.setModal(true);
				etlPeriodicityJDialog.setVisible(true);
			}
		});
		
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblEstadoDelAnlisis)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnAnalysisStatus)))
					.addGap(22)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnModificarPeriodicidad, GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEstadoDelAnlisis, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAnalysisStatus, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addGap(0, 0, Short.MAX_VALUE))
				.addComponent(separator, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnModificarPeriodicidad, GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		connectionRecordsTable = new JTable();
		connectionRecordsTable.setFillsViewportHeight(true);
		connectionRecordsTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"IP origen", "IP destino", "fecha de ejecuci\u00F3n", "Hora", "Estado"
			}
		));
		loadData();
		scrollPane.setViewportView(connectionRecordsTable);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);

        hilo.start();
	}
	public void run() {
		int time = 1000;
		
        try {
	        while (true) {
	        	
				Thread.sleep( time );
				DateTime newtUpdateDateTime = new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(), MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute(), MainDataStatic.nextUpdate().getSecond());
								
				Period period = MainDataStatic.nextUpdatePeriod();
				
				if (MainDataStatic.analysis_status && newtUpdateDateTime.isAfterNow()) {
		    		lblTiempoRestante.setText("Tiempo restante: "+Math.abs(period.getDays())+" D�as, "+Math.abs(period.getHours())+" horas y  "+Math.abs(period.getMinutes())+" minutos con "+Math.abs(period.getSeconds())+" segundos");
				}
				else if (MainDataStatic.analysis_status && newtUpdateDateTime.isBeforeNow()) {
					lblTiempoRestante.setText("Tiempo expirado hace: "+Math.abs(period.getDays())+" D�as, "+Math.abs(period.getHours())+" horas y  "+Math.abs(period.getMinutes())+" minutos con "+Math.abs(period.getSeconds())+" segundos");
				}
				else {
					lblTiempoRestante.setText("Tiempo restante: El an�lisis ha sido desactivado.");
				}

			    lblLastUpdate.setText("�ltima actualizaci�n: "+MainDataStatic.lastUpdate().getYear()+"-"+MainDataStatic.lastUpdate().getMonth()+"-"+MainDataStatic.lastUpdate().getDay()+", Hora: "+MainDataStatic.lastUpdate().getHour()+":"+MainDataStatic.lastUpdate().getSecond()+".");
			    lblNextUpdate.setText("Pr�xima actualizaci�n: "+MainDataStatic.nextUpdate().getYear()+"-"+MainDataStatic.nextUpdate().getMonth()+"-"+MainDataStatic.nextUpdate().getDay()+", Hora: "+MainDataStatic.nextUpdate().getHour()+":"+MainDataStatic.nextUpdate().getSecond()+".");
	        }
		} catch (InterruptedException e) {
			JOptionPane.showMessageDialog(panel, "Error: "+e.getMessage().toString());
			lblTiempoRestante.setText("Tiempo restante: 0 D\u00EDas, 0 horas y  0 minutos con 0 segundos.");
		}
	}
	public void loadData() {
		DefaultTableModel defaultTableModel = new DefaultTableModel(
				new Object[][] {
					
				},
				new String[] {
						"IP origen", "IP destino", "fecha de ejecuci\u00F3n", "Hora", "Estado"
				}
		);
		
		for (ConnectionRecordModel connectionRecordModel: MainDataStatic.connectionRecordsModel) {
			String fecha = connectionRecordModel.getExecutionDateModel().getYear()+"-"+connectionRecordModel.getExecutionDateModel().getMonth()+"-"+connectionRecordModel.getExecutionDateModel().getDay();
			String hora = connectionRecordModel.getExecutionDateModel().getHour()+":"+connectionRecordModel.getExecutionDateModel().getMinute()+":"+connectionRecordModel.getExecutionDateModel().getSecond();
			
			Object[] data = {connectionRecordModel.getIp_origen(), connectionRecordModel.getIp_destino(), fecha, hora, connectionRecordModel.getEstado()};
			defaultTableModel.addRow(data);
		}
		connectionRecordsTable.setModel(defaultTableModel);
	}
}

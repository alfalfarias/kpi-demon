package views;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import org.joda.time.DateTime;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import examples.MySQL;
import models.ConnectionDBModel;
import models.MainDataStatic;
import models.PeriodCount;

import javax.swing.JPasswordField;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import java.awt.Toolkit;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.SpinnerNumberModel;

public class EtlPeriodicityJDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8246469106196920938L;
	private final JPanel contentPanel = new JPanel();
	private JTextField portExtraccionTextField;
	private JTextField hostExtraccionTextField;
	private JTextField databaseExtraccionTextField;
	private JTextField usernameExtraccionTextField;
	private JPasswordField passwordExtraccionPasswordField;
	private JTextField portCargaTextField;
	private JTextField hostCargaTextField;
	private JTextField databaseCargaTextField;
	private JTextField usernameCargaTextField;
	private JPasswordField passwordCargaPasswordField;
	private JProgressBar progressBar = new JProgressBar(0,100);
	private JSpinner daysCountSpinner;
	private JSpinner hoursCountSpinner;
	private JSpinner minutesCountSpinner;
	private JLabel lblLoading;
	private JDateChooser dateChooser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EtlPeriodicityJDialog dialog = new EtlPeriodicityJDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EtlPeriodicityJDialog() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(EtlPeriodicityJDialog.class.getResource("/img/data-storage.png")));
		setTitle("Modificar periodicidad");
		setBounds(100, 100, 691, 401);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Datos de Periodicidad", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Configuraci\u00F3n de conexi\u00F3n a la DB", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
						.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "DB extracci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JLabel label = new JLabel("Host (ingrese el URL):");
		
		JLabel label_1 = new JLabel("Puerto (MySQL: 3306):");
		
		JLabel label_2 = new JLabel("Contrase\u00F1a:");
		
		JLabel label_3 = new JLabel("Nombre de Usuario:");
		
		JLabel lblNombreDeLa = new JLabel("Nombre de la DB:");
		
		portExtraccionTextField = new JTextField();
		portExtraccionTextField.setText((String) null);
		portExtraccionTextField.setColumns(10);
		
		hostExtraccionTextField = new JTextField();
		hostExtraccionTextField.setText((String) null);
		hostExtraccionTextField.setColumns(10);
		
		databaseExtraccionTextField = new JTextField();
		databaseExtraccionTextField.setText((String) null);
		databaseExtraccionTextField.setColumns(10);
		
		usernameExtraccionTextField = new JTextField();
		usernameExtraccionTextField.setText((String) null);
		usernameExtraccionTextField.setColumns(10);
		
		passwordExtraccionPasswordField = new JPasswordField((String) null);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
									.addComponent(label, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
									.addComponent(label_1))
								.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
							.addGap(22))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblNombreDeLa, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)))
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(portExtraccionTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(hostExtraccionTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(databaseExtraccionTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(usernameExtraccionTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(passwordExtraccionPasswordField))
					.addGap(17))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addComponent(hostExtraccionTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(portExtraccionTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(databaseExtraccionTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNombreDeLa))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(usernameExtraccionTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2)
						.addComponent(passwordExtraccionPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(12, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "DB carga", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JLabel label_5 = new JLabel("Host (ingrese el URL):");
		
		JLabel label_6 = new JLabel("Puerto (MySQL: 3306):");
		
		JLabel label_7 = new JLabel("Contrase\u00F1a:");
		
		JLabel label_8 = new JLabel("Nombre de Usuario:");
		
		JLabel label_9 = new JLabel("Nombre de la Base de Datos:");
		
		portCargaTextField = new JTextField();
		portCargaTextField.setText((String) null);
		portCargaTextField.setColumns(10);
		
		hostCargaTextField = new JTextField();
		hostCargaTextField.setText((String) null);
		hostCargaTextField.setColumns(10);
		
		databaseCargaTextField = new JTextField();
		databaseCargaTextField.setText((String) null);
		databaseCargaTextField.setColumns(10);
		
		usernameCargaTextField = new JTextField();
		usernameCargaTextField.setText((String) null);
		usernameCargaTextField.setColumns(10);
		
		passwordCargaPasswordField = new JPasswordField((String) null);
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 313, Short.MAX_VALUE)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_3.createSequentialGroup()
							.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING, false)
									.addComponent(label_5, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
									.addComponent(label_6))
								.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
							.addGap(22))
						.addGroup(gl_panel_3.createSequentialGroup()
							.addComponent(label_9, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)))
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addComponent(portCargaTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(hostCargaTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(databaseCargaTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(usernameCargaTextField, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
						.addComponent(passwordCargaPasswordField))
					.addGap(17))
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGap(0, 162, Short.MAX_VALUE)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
						.addComponent(hostCargaTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(portCargaTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_6))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(databaseCargaTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_9))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(usernameCargaTextField, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_8))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7)
						.addComponent(passwordCargaPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(12, Short.MAX_VALUE))
		);
		panel_3.setLayout(gl_panel_3);
		
		JButton btnPingDbExtraccin = new JButton("Ping DB Extracci\u00F3n");
		btnPingDbExtraccin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConnectionDBModel extraccionCDBModel = new ConnectionDBModel(hostExtraccionTextField.getText(), portExtraccionTextField.getText(), databaseExtraccionTextField.getText(), usernameExtraccionTextField.getText(), String.valueOf(passwordExtraccionPasswordField.getPassword()));
				try {
					for (Field field : extraccionCDBModel.getClass().getDeclaredFields()) {
					    field.setAccessible(true);
					    String name = field.getName();
					    Object value = field.get(extraccionCDBModel);
					    System.out.printf("%s: %s%n", name, value);
					}
					
					MySQL.pingConnect(extraccionCDBModel);
					JOptionPane.showMessageDialog(
							   contentPanel,
							   "�Conexi�n a la DB de extracci�n realizada con �xito!");
				} catch (HeadlessException | ClassNotFoundException | SQLException | IllegalArgumentException | IllegalAccessException e) {
					JOptionPane.showMessageDialog(
							   contentPanel,
							   "Error: "+e.getMessage());
				} 
			}
		});
		
		JButton btnPingDbCarga = new JButton("Ping DB Carga");
		btnPingDbCarga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConnectionDBModel cargaCDBModel = new ConnectionDBModel(hostCargaTextField.getText(), portCargaTextField.getText(), databaseCargaTextField.getText(), usernameCargaTextField.getText(), String.valueOf(passwordCargaPasswordField.getPassword()));
				try {
					MySQL.pingConnect(cargaCDBModel);
					JOptionPane.showMessageDialog(
							   contentPanel,
							   "�Conexi�n a la DB de carga realizada con �xito!");
				} catch (HeadlessException | ClassNotFoundException | SQLException e) {
					JOptionPane.showMessageDialog(
							   contentPanel,
							   "Error: "+e.getMessage());
				} 
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(btnPingDbExtraccin, GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(btnPingDbCarga, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
						.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(panel_3, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
						.addComponent(panel_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnPingDbExtraccin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnPingDbCarga, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(8))
		);
		panel_1.setLayout(gl_panel_1);

		dateChooser = new JDateChooser((new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(),MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute())).toDate());
		 
		JLabel lblDias = new JLabel("Dias:");
		
		daysCountSpinner = new JSpinner();
		daysCountSpinner.setModel(new SpinnerNumberModel(0, 0, null, 1));
		daysCountSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				DateTime dateTime = new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(), MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute(), MainDataStatic.nextUpdate().getSecond());
				dateTime = dateTime.plusDays(Integer.parseInt(daysCountSpinner.getValue().toString()));
				dateChooser.setDate(dateTime.toDate());				
//				System.out.println("A�o: "+dateTime.getYear());
//				System.out.println("Mes: "+dateTime.getMonthOfYear());
//				System.out.println("D�a: "+dateTime.getDayOfMonth());
//				System.out.println("Minuto: "+dateTime.getMinuteOfHour());
//				System.out.println("Segundo: "+dateTime.getSecondOfMinute());
//				System.out.print("VALOR DEL CONTADOR: "+Integer.parseInt(daysCountSpinner.getValue().toString()));
			}
		});

		JLabel lblHoras = new JLabel("Horas:");
		
		hoursCountSpinner = new JSpinner();
		hoursCountSpinner.setModel(new SpinnerNumberModel(13, 0, 23, 1));
		hoursCountSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				DateTime dateTime = new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(), MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute(), MainDataStatic.nextUpdate().getSecond());
				dateTime = dateTime.plusHours(Integer.parseInt(hoursCountSpinner.getValue().toString()));
				dateChooser.setDate(dateTime.toDate());
			}
		});
		
		JLabel lblMinutos = new JLabel("Minutos:");
		
		minutesCountSpinner = new JSpinner();
		minutesCountSpinner.setModel(new SpinnerNumberModel(30, 0, 59, 1));
		minutesCountSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				DateTime dateTime = new DateTime(MainDataStatic.nextUpdate().getYear(), MainDataStatic.nextUpdate().getMonth(), MainDataStatic.nextUpdate().getDay(), MainDataStatic.nextUpdate().getHour(), MainDataStatic.nextUpdate().getMinute(), MainDataStatic.nextUpdate().getSecond());
				dateTime = dateTime.plusMinutes(Integer.parseInt(minutesCountSpinner.getValue().toString()));
				dateChooser.setDate(dateTime.toDate());
			}
		});

		daysCountSpinner.setValue(MainDataStatic.getPeriodCount().getDays());
		hoursCountSpinner.setValue(MainDataStatic.getPeriodCount().getHours());
		minutesCountSpinner.setValue(MainDataStatic.getPeriodCount().getMinutes());
		
		JLabel lblFechaDeActualizacin = new JLabel("Fecha para la proxima actualizaci�n:");
		
		JLabel lblltimaActualizacin = new JLabel("�ltima actualizaci�n: "+MainDataStatic.lastUpdate().getYear()+"-"+MainDataStatic.lastUpdate().getMonth()+"-"+MainDataStatic.lastUpdate().getDay()+", Hora: "+MainDataStatic.lastUpdate().getHour()+":"+MainDataStatic.lastUpdate().getMinute()+":"+MainDataStatic.lastUpdate().getSecond()+".");
		lblltimaActualizacin.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(lblDias)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(daysCountSpinner, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblHoras)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(hoursCountSpinner, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblMinutos)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(minutesCountSpinner, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblFechaDeActualizacin)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(dateChooser, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
						.addComponent(lblltimaActualizacin, GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblDias)
							.addComponent(daysCountSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(hoursCountSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(3)
							.addComponent(lblHoras))
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(minutesCountSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblFechaDeActualizacin))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(3)
							.addComponent(lblMinutos))
						.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblltimaActualizacin)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Guardar cambios");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						PeriodCount periodCount = new PeriodCount(Integer.parseInt(daysCountSpinner.getValue().toString()), Integer.parseInt(hoursCountSpinner.getValue().toString()), Integer.parseInt(minutesCountSpinner.getValue().toString()), 0);
						ConnectionDBModel extraccionCDBModel = new ConnectionDBModel(hostExtraccionTextField.getText(), portExtraccionTextField.getText(), databaseExtraccionTextField.getText(), usernameExtraccionTextField.getText(), String.valueOf(passwordExtraccionPasswordField.getPassword()));
						ConnectionDBModel cargaCDBModel = new ConnectionDBModel(hostCargaTextField.getText(), portCargaTextField.getText(), databaseCargaTextField.getText(), usernameCargaTextField.getText(), String.valueOf(passwordCargaPasswordField.getPassword()));
						
						int confirmado = JOptionPane.showConfirmDialog(
								   contentPanel,
								   "�Seguro que desea realizar los cambios?");
						if (JOptionPane.OK_OPTION == confirmado){
							int total_load=100;
							try {
								lblLoading.setText("Estableciendo conexi�n con el Servidor de SIGO");
								System.out.print("Estableciendo conexi�n con el Servidor de SIGO");
								Thread.sleep(1000);
								progressBar.setValue(total_load/4);
								if (MySQL.pingConnect(extraccionCDBModel)) {
									lblLoading.setText("1) Conexi�n a la base de datos de EXTRACCI�N realizada con �xito");
									System.out.print("1) Conexi�n a la base de datos de EXTRACCI�N realizada con �xito");
									Thread.sleep(1000);
									progressBar.setValue(total_load/3);
								}
								lblLoading.setText("Estableciendo conexi�n con el Servidor de SIGI");
								System.out.print("Estableciendo conexi�n con el Servidor de SIGI");
								progressBar.setValue(total_load/2);
								Thread.sleep(1000);
								if (MySQL.pingConnect(cargaCDBModel)) {
									lblLoading.setText("2) Conexi�n a la base de datos de CARGA realizada con �xito");
									System.out.print("2) Conexi�n a la base de datos de CARGA realizada con �xito");
									progressBar.setValue(total_load);
								}
								MainDataStatic.setPeriodCount(periodCount);
								MainDataStatic.setExtraccionCDBModel(extraccionCDBModel);
								MainDataStatic.setCargaCDBModel(cargaCDBModel);
								JOptionPane.showMessageDialog(
										   contentPanel,
										   "�Conexi�n a la realizada con �xito!");
								dispose();
							} catch (ClassNotFoundException | SQLException | InterruptedException e) {
								JOptionPane.showMessageDialog(contentPanel, "Error: "+e.getMessage().toString());
							}
						}
					}
				});
				{
					JButton cancelButton = new JButton("Cancelar");
					cancelButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							dispose();
						}
					});
					
					lblLoading = new JLabel("");
					buttonPane.add(lblLoading);
					
					progressBar = new JProgressBar();
					buttonPane.add(progressBar);
					cancelButton.setActionCommand("Cancel");
					buttonPane.add(cancelButton);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			hostExtraccionTextField.setText(MainDataStatic.extraccionCDBModel.host);
			portExtraccionTextField.setText(MainDataStatic.extraccionCDBModel.port);
			databaseExtraccionTextField.setText(MainDataStatic.extraccionCDBModel.database);
			usernameExtraccionTextField.setText(MainDataStatic.extraccionCDBModel.username);
			passwordExtraccionPasswordField.setText(MainDataStatic.extraccionCDBModel.password);
		}
		{
			hostCargaTextField.setText(MainDataStatic.cargaCDBModel.host);
			portCargaTextField.setText(MainDataStatic.cargaCDBModel.port);
			databaseCargaTextField.setText(MainDataStatic.cargaCDBModel.database);
			usernameCargaTextField.setText(MainDataStatic.cargaCDBModel.username);
			passwordCargaPasswordField.setText(MainDataStatic.cargaCDBModel.password);
		}
	}
}

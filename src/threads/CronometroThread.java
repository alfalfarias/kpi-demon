package threads;

public class CronometroThread implements Runnable {
	private Thread thread;
	public CronometroThread() {
		thread = new Thread (this);
        thread.start ();
	}
	@Override
	public void run() {
	      System.out.println("Running T1"  );
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread: T1." + i);
	            // Let the thread sleep for a while.
	            Thread.sleep(50);
	         }
	      }catch (InterruptedException e) {
	         System.out.println("Thread T1 interrupted.");
	      }
	      System.out.println("Thread T1 exiting.");
	}
	
}

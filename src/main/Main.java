package main;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

import org.joda.time.Seconds;

import models.MainDataStatic;
import views.EtlJFrame;
 
/**
 *
 * @author Jonathan
 */
public class Main implements Runnable {
	static EtlJFrame frame;
	static TrayIcon icono; 
	
	public Main() throws AWTException, InterruptedException {
		LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e1) {
			try {
				UIManager.setLookAndFeel(lookAndFeel);
			} catch (UnsupportedLookAndFeelException e11) {
				JOptionPane.showMessageDialog(
						   null,
						   "Error: "+e11.getMessage().toString());
			}
		}  
		
		frame = new EtlJFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		icono = new TrayIcon(Toolkit.getDefaultToolkit().getImage(System.getProperty("user.dir") + "\\src\\img\\settings.png"),"ETL - Extracci�n, transformaci�n y carga",crearMenu());
        SystemTray.getSystemTray().add(icono);
        Thread.sleep(1000);
        icono.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
        		frame.setVisible(true);
            }
        });
        icono.displayMessage("ETL - SIGI", "Inicializando aplicaci�n", TrayIcon.MessageType.INFO);
        etlStatus();

        
		frame.setVisible(true);
	}
	
    public static void main(String args[]) throws Exception{
		
//        TrayIcon icono = new TrayIcon(Toolkit.getDefaultToolkit().getImage(EtlJFrame.class.getResource("/img/data-storage.png")));
        Main main = new Main();
        main.run();

        
    }
    public void etlStatus() throws InterruptedException{
    	Thread.sleep(2000);
    	if (MainDataStatic.isAnalysisStatus()) {
    		icono.displayMessage("ETL - En ejecuci�n", "Clickea aqu� para m�s informaci�n", TrayIcon.MessageType.INFO);
    	}
	    else {
	        icono.displayMessage("ETL - En pausa", "El ETL se ha detenido", TrayIcon.MessageType.INFO);
	    }
    }
    public static PopupMenu crearMenu(){
        PopupMenu menu = new PopupMenu();
        MenuItem analisisMenuItem = new MenuItem("Ver estado del an�lisis");
        MenuItem salirMenuItem = new MenuItem("Salir");

        analisisMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){	
            	frame.setVisible(true);

            }
        });
        salirMenuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
            	int confirmado = JOptionPane.showConfirmDialog(
						   frame,
						   "�Seguro que desea salir del ETL? \n (Se detrendr� la migraci�n de datos)");
				if (JOptionPane.OK_OPTION == confirmado){
	                System.exit(0);
				}
            }
        });

        menu.add(analisisMenuItem);
        menu.add(salirMenuItem);
        return menu;
    }

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Hacer las migraciones
			if (MainDataStatic.isAnalysisStatus()) {
				if (Seconds.standardSecondsIn(MainDataStatic.nextUpdatePeriod()).getSeconds() <= 0) {
					MainDataStatic.migrate();
			        icono.displayMessage("ETL - Migrando", "From IP: "+MainDataStatic.getExtraccionCDBModel().host, TrayIcon.MessageType.INFO);
			        icono.displayMessage("ETL - Migraci�n realizada con �xito", "To IP: "+MainDataStatic.getCargaCDBModel().host, TrayIcon.MessageType.INFO);
					frame.loadData();
				}
				else {
					System.out.println(Seconds.standardSecondsIn(MainDataStatic.nextUpdatePeriod()).getSeconds());
				}
			}
			else {
//				System.out.println("Analisis desactivado. Segundos: "+Seconds.standardSecondsIn(MainDataStatic.nextUpdatePeriod()).getSeconds());
			}
		}
	}
}